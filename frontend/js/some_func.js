// # TEMPLATE
( function MyApp(){
  // console.log('init function');
  // https://www.json-generator.com/#
    var input = document.querySelector("input"),
        btn = document.querySelector("#btn");

    btn.addEventListener('click', isValidUrl);

    function isValidUrl(){
        var objRE = /https?:\/\/[\w\d:\.:\?\&]+/g;
        var urlLink = input.value;
        if(objRE.test(urlLink) === true){
            choose();
        }else {
            var ul = document.createElement("ul");
            document.body.appendChild(ul);
            var li = document.createElement("li");
            li.innerHTML = "Error link";
            li.style.color = "red";
            li.style.listStyleType = "none";
            document.querySelector("ul").appendChild(li);
        }
    }
    function choose() {
    var fetchedData = fetch(input.value).then(function(response) {
    return response.json();
    }).then(data => {
    renderInterface(data);
    })};
    }());


    function renderInterface( data ){
        data.map(function (arr) {
            if(arr.favoriteFruit === "banana"){
                elementObj(arr);
            }else if(arr.balance.slice(1,-1).replace(",","")>2000 && arr.age > 25){
                elementObj(arr);
            }else if (arr.eyeColor === "blue" || arr.gender === "female" && arr.isActive === false){
                elementObj(arr);
            }
        });

    function elementObj(element) {
        var div = document.createElement("div");
        document.body.appendChild(div);
        var newUl = document.createElement("ul");
        div.style.width = "50%";
        div.style.float = "left";
        div.appendChild(newUl);
        var ul = document.querySelector("ul");
            for(var key in element){
            var li = document.createElement("li");
            li.innerHTML = key + " = " + element[key];
            li.style.color = "green";
            li.style.listStyleType = "none";
            ul.appendChild(li);
            }
            li.innerHTML="---------------------------------------------------"

    }
}
//
// // Задача:
// // Написать ф-ю которая принимает на вход обьект с сервера и
// // разбить его на 3 массива по параметрам описаным ниже.
// // + бонус вывести каждый список на экран
// // + бонус 2 сделать поле инпута куда вставить ссылку с json-generator
// // для перерендера списка по клику на кнопку
// // + бонус 3 если вставить не валидную ссылку выводить ошибку
//
// // # = условие -> вывод
// // array 1 = fruit == banana -> name,
// // array 2 = balance > 2000, age > 25
// // array 3 = eyeColor === blue, gender === female, isActive === false
